#Prueba definicion de una funcion y ejecucion
def solucion(string1, string2):
    diccionario = {}
    for char in string1:
        if(char in diccionario):
            diccionario[char] += 1
        else:
            diccionario[char] = 1
    for char in string2:
        if(char in diccionario) and (diccionario[char] > 0):
            diccionario[char] -= 1
        else:
            return False


    return True


print(solucion("listadecaracteres", "caracterlist"))
print(solucion("lista","error"))